const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const listingSchema = new Schema({
    sn: Number,
    class: String,
    subject: String,
    chapter_name: String,
    lecture_number: Number,
    video_link: String

});

const Listing = mongoose.model("Listing", listingSchema);

module.exports = Listing;