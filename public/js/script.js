// for validation
(() => {
    'use strict'
  
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    const forms = document.querySelectorAll('.needs-validation')
  
    // Loop over them and prevent submission
    Array.from(forms).forEach(form => {
      form.addEventListener('submit', event => {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
  
        form.classList.add('was-validated')
      }, false)
    })
})()



// search box 

function myFunction() {
  // Declare variables
  var input, filter, table, tr, td, i, j, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, starting from index 1 to skip the header
  for (i = 1; i < tr.length; i++) {
    // Loop through all table cells in the current row
    var rowVisible = false;
    for (j = 0; j < tr[i].cells.length; j++) {
      td = tr[i].cells[j];
      if (td) {
        txtValue = td.textContent || td.innerText;
        // Check if any cell content contains the filter string
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          rowVisible = true;
          break; // Break the inner loop if a match is found in any cell
        }
      }
    }

    // Show or hide the row based on the overall result
    if (rowVisible) {
      tr[i].style.display = "";
    } else {
      tr[i].style.display = "none";
    }
  }
}

//Gallery Autoslider

document.addEventListener("DOMContentLoaded", function() {
  const slider = document.querySelector(".gallery-content");
  let counter = 1;

  setInterval(() => {
      slider.style.transition = "transform 0.8s ease-in-out";
      slider.style.transform = `translateX(${-counter * 100}%)`;

      counter++;
      if (counter === slider.children.length) {
          setTimeout(() => {
              slider.style.transition = "none";
              slider.style.transform = "translateX(0)";
              counter = 1;
          }, 800);
      }
  }, 3000); // Change the time interval (in milliseconds) for auto-sliding
});


// Change content on dropdown Selection

function filterTable1() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("class");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the filter
  for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[1]; // Assuming the category is in the second column

      if (td) {
          txtValue = td.textContent || td.innerText;
          if (filter === "all" || txtValue.toUpperCase() === filter) {
              tr[i].style.display = "";
          } else {
              tr[i].style.display = "none";
          }
      }
  }
}

function filterTable2() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("subject");
  input2 = document.getElementById("class");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[2];

      if (td) {
          txtValue = td.textContent || td.innerText;
          if (filter === "all" || txtValue.toUpperCase() === filter) {
              tr[i].style.display = "";
          } else {
              tr[i].style.display = "none";
          }
      }
  }
}



// Onclick Video Change
function changeVideo(videoUrl) {
  videoUrl = videoUrl + "?autoplay=1&mute=0";
  document.getElementById("videoBox").src = videoUrl;
  // Prevent the default behavior of the anchor tag (prevents navigating to a new page)
  event.preventDefault();
}

