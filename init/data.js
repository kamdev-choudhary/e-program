const classXData = [
     {
        sn: 1, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 1, video_link: "https://youtube.com/embed/hpRGxDyQpiE"
    },
    {
        sn: 2, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 2, video_link: "https://youtube.com/embed/jGncR7ytaeo"},
    {
        sn: 3, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 3, video_link: "https://youtube.com/embed/6tuEjcY_be0"
    },
    {
        sn: 4, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 4, video_link: "https://youtube.com/embed/LMRumXsytIw"
    },
    {
        sn: 5, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 5, video_link: "https://youtube.com/embed/P_0v6KJl8bs"
    },
    {
        sn: 6, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 6, video_link: "https://youtube.com/embed/PE37E78ZaPU"
    },
    {
        sn: 7, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 7, video_link: "https://youtube.com/embed/geK2824Tons"
    },
    {
        sn: 8, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 8, video_link: "https://youtube.com/embed/sHe5-Ats3Yw"
    },
    {
        sn: 9, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 9, video_link: "https://youtube.com/embed/3RGoYeLj2xk"
    },
    {
        sn: 10, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 10, video_link: "https://youtube.com/embed/VZojCpG9zU4"
    },
    {
        sn: 11, class: "X", subject: "Physics", chapter_name: "Light - Reflection and Refraction", lecture_number: 11, video_link: "https://youtube.com/embed/KoSKqQZ4UR4"
    },
    {
        sn: 12, class: "X", subject: "Physics", chapter_name: "Human Eye and Colourful World", lecture_number: 1, video_link: "https://youtube.com/embed/IdB05ZXE_Gk"
    },
    {
        sn: 13, class: "X", subject: "Physics", chapter_name: "Human Eye and Colourful World", lecture_number: 2, video_link: "https://youtube.com/embed/Q-PEmcZXhEw"
    },
    {
        sn: 14, class: "X", subject: "Physics", chapter_name: "Human Eye and Colourful World", lecture_number: 3, video_link: "https://youtube.com/embed/o1cxV3lNcvM"
    },
    {
        sn: 15, class: "X", subject: "Physics", chapter_name: "Human Eye and Colourful World", lecture_number: 4, video_link: "https://youtube.com/embed/n6S_rTRIxS4"
    },
    {
        sn: 16, class: "X", subject: "Physics", chapter_name: "Human Eye and Colourful World", lecture_number: 5, video_link: "https://youtube.com/embed/P9gvbJaJJjA"
    },
    {
        sn: 17, class: "X", subject: "Physics", chapter_name: "Human Eye and Colourful World", lecture_number: 6, video_link: "https://youtube.com/embed/NpdKiYJCjdA"
    },
    {
        sn: 18, class: "X", subject: "Physics", chapter_name: "Human Eye and Colourful World", lecture_number: 7, video_link: "https://youtube.com/embed/3Vv4X6IiJHs"
    },
    {
        sn: 19, class: "X", subject: "Physics", chapter_name: "Human Eye and Colourful World", lecture_number: 8, video_link: "https://youtube.com/embed/Mxm4eoyk_rI"
    },
    {
        sn: 20, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 1, video_link: "https://youtube.com/embed/NP3YUu7T3IY"
    },
    {
        sn: 21, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 2, video_link: "https://youtube.com/embed/HOmewsD9zIU"
    },
    {
        sn: 22, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 3, video_link: "https://youtube.com/embed/YACICckVYdM"
    },
    {
        sn: 23, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 4, video_link: "https://youtube.com/embed/VNKo3hwiQk0"
    },
    {
        sn: 24, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 5, video_link: "https://youtube.com/embed/uCrKlf8F9ZY"
    },
    {
        sn: 25, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 6, video_link: "https://youtube.com/embed/D7DCjI-PG64"
    },
    {
        sn: 26, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 7, video_link: "https://youtube.com/embed/UWQU9QojhAA"
    },
    {
        sn: 27, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 8, video_link: "https://youtube.com/embed/zT52HMxbGNk"
    },
    {
        sn: 28, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 9, video_link: "https://youtube.com/embed/0YXooLhYRb8"
    },
    {
        sn: 29, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 10, video_link: "https://youtube.com/embed/jQf8_fMq4rU"
    },
    {
        sn: 30, class: "X", subject: "Physics", chapter_name: "Electricity", lecture_number: 11, video_link: "https://youtube.com/embed/yAoIQ6Ayrvc"
    },
    {
        sn: 31, class: "X", subject: "Physics", chapter_name: "Magnetic Effects of Electric Current", lecture_number: 1, video_link: "https://youtube.com/embed/-d6p-hPCSjA"
    },
    {
        sn: 32, class: "X", subject: "Physics", chapter_name: "Magnetic Effects of Electric Current", lecture_number: 2, video_link: "https://youtube.com/embed/NnEXdZHOcRU"
    },
    {
        sn: 33, class: "X", subject: "Physics", chapter_name: "Magnetic Effects of Electric Current", lecture_number: 3, video_link: "https://youtube.com/embed/k5Fg8DwFZaU"
    },
    {
        sn: 34, class: "X", subject: "Physics", chapter_name: "Magnetic Effects of Electric Current", lecture_number: 4, video_link: "https://youtube.com/embed/YBjDvHJcGf8"
    },
    {
        sn: 35, class: "X", subject: "Physics", chapter_name: "Magnetic Effects of Electric Current", lecture_number: 5, video_link: "https://youtube.com/embed/xXWXvJBv6Ms"
    },
    {
        sn: 36, class: "X", subject: "Physics", chapter_name: "Magnetic Effects of Electric Current", lecture_number: 6, video_link: "https://youtube.com/embed/wSAzWwNcG0M"
    },
    {
        sn: 37, class: "X", subject: "Physics", chapter_name: "Magnetic Effects of Electric Current", lecture_number: 7, video_link: "https://youtube.com/embed/EGcXMUj7VTg"
    },
    {
        sn: 38, class: "X", subject: "Physics", chapter_name: "Magnetic Effects of Electric Current", lecture_number: 8, video_link: "https://youtube.com/embed/wc9bzduAgSU"
    },
    {
        sn: 39, class: "X", subject: "Physics", chapter_name: "Sources of Energy", lecture_number: 1, video_link: "https://youtube.com/embed/pMhq4z1GocI"
    },
    {
        sn: 40, class: "X", subject: "Physics", chapter_name: "Sources of Energy", lecture_number: 2, video_link: "https://youtube.com/embed/bQqbzWy7kPo"
    },
    {
        sn: 41, class: "X", subject: "Physics", chapter_name: "Sources of Energy", lecture_number: 3, video_link: "https://youtube.com/embed/oCtRMbroavk"
    },
    {
        sn: 42, class: "X", subject: "Physics", chapter_name: "Sources of Energy", lecture_number: 4, video_link: "https://youtube.com/embed/CQt7SB1MqcM"
    },
    {
        sn: 43, class: "X", subject: "Physics", chapter_name: "Sources of Energy", lecture_number: 5, video_link: "https://youtube.com/embed/sMHdsq6Fz-U"
    },
    {
        sn: 44, class: "X", subject: "Chemistry", chapter_name: "Chemical Reactions and Equations", lecture_number: 1, video_link: "https://youtube.com/embed/IQzw5r4H1pA"
    },
    {
        sn: 45, class: "X", subject: "Chemistry", chapter_name: "Chemical Reactions and Equations", lecture_number: 2, video_link: "https://youtube.com/embed/zCjw3tarK9o"
    },
    {
        sn: 46, class: "X", subject: "Chemistry", chapter_name: "Chemical Reactions and Equations", lecture_number: 3, video_link: "https://youtube.com/embed/zZzJZTNcG8E"
    },
    {
        sn: 47, class: "X", subject: "Chemistry", chapter_name: "Chemical Reactions and Equations", lecture_number: 4, video_link: "https://youtube.com/embed/T1J2-WQLwRQ"
    },
    {
        sn: 48, class: "X", subject: "Chemistry", chapter_name: "Chemical Reactions and Equations", lecture_number: 5, video_link: "https://youtube.com/embed/sjN9cQng5jQ"
    },
    {
        sn: 49, class: "X", subject: "Chemistry", chapter_name: "Chemical Reactions and Equations", lecture_number: 6, video_link: "https://youtube.com/embed/EeVumV3w_Do"
    },
    {
        sn: 50, class: "X", subject: "Chemistry", chapter_name: "Chemical Reactions and Equations", lecture_number: 7, video_link: "https://youtube.com/embed/g5SBp7K89Wk"
    },
    {
        sn: 51, class: "X", subject: "Chemistry", chapter_name: "Chemical Reactions and Equations", lecture_number: 8, video_link: "https://youtube.com/embed/LDzVmD9M_lE"
    },
    {
        sn: 52, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 1, video_link: "https://youtube.com/embed/6CBZFQRbx4w"
    },
    {
        sn: 53, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 2, video_link: "https://youtube.com/embed/jy94ASlKRC0"
    },
    {
        sn: 54, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 3, video_link: "https://youtube.com/embed/HlbW5hQ2h1w"
    },
    {
        sn: 55, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 4, video_link: "https://youtube.com/embed/H59KiCr503M"
    },
    {
        sn: 56, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 5, video_link: "https://youtube.com/embed/27jRMUyltiw"
    },
    {
        sn: 57, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 6, video_link: "https://youtube.com/embed/ilXQYudWB04"
    },
    {
        sn: 58, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 7, video_link: "https://youtube.com/embed/RqnioPGKeZQ"
    },
    {
        sn: 59, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 8, video_link: "https://youtube.com/embed/FGxFExz0Q_M"
    },
    {
        sn: 60, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 9, video_link: "https://youtube.com/embed/wwO7tuGWbR8"
    },
    {
        sn: 61, class: "X", subject: "Chemistry", chapter_name: "Acids, bases and salts", lecture_number: 10, video_link: "https://youtube.com/embed/upQMbUvdE0g"
    },
    {
        sn: 62, class: "X", subject: "Chemistry", chapter_name: "Metals and Non-Metals", lecture_number: 1, video_link: "https://youtube.com/embed/euZVypINNjo"
    },
    {
        sn: 63, class: "X", subject: "Chemistry", chapter_name: "Metals and Non-Metals", lecture_number: 2, video_link: "https://youtube.com/embed/UJBQQ1XbY08"
    },
    {
        sn: 64, class: "X", subject: "Chemistry", chapter_name: "Metals and Non-Metals", lecture_number: 3, video_link: "https://youtube.com/embed/SKWvYwZQc9E"
    },
    {
        sn: 65, class: "X", subject: "Chemistry", chapter_name: "Metals and Non-Metals", lecture_number: 4, video_link: "https://youtube.com/embed/zdzm48ytQCQ"
    },
    {
        sn: 66, class: "X", subject: "Chemistry", chapter_name: "Metals and Non-Metals", lecture_number: 5, video_link: "https://youtube.com/embed/T07q_v2mMfc"
    },
    {
        sn: 67, class: "X", subject: "Chemistry", chapter_name: "Metals and Non-Metals", lecture_number: 6, video_link: "https://youtube.com/embed/qyxo-x5Lxx0"
    },
    {
        sn: 68, class: "X", subject: "Chemistry", chapter_name: "Metals and Non-Metals", lecture_number: 7, video_link: "https://youtube.com/embed/zTpSTwx_cwM"
    },
    {
        sn: 69, class: "X", subject: "Chemistry", chapter_name: "Metals and Non-Metals", lecture_number: 8, video_link: "https://youtube.com/embed/fJG86mxCNH4"
    },
    {
        sn: 70, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 1, video_link: "https://youtube.com/embed/Z751hJk3ifo"
    },
    {
        sn: 71, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 2, video_link: "https://youtube.com/embed/DyulfbupSXI"
    },
    {
        sn: 72, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 3, video_link: "https://youtube.com/embed/-dIFhM7o6So"
    },
    {
        sn: 73, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 4, video_link: "https://youtube.com/embed/l841T9KkB8Y"
    },
    {
        sn: 74, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 5, video_link: "https://youtube.com/embed/yQFA-JwLP5A"
    },
    {
        sn: 75, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 6, video_link: "https://youtube.com/embed/0x0s78SvXTQ"
    },
    {
        sn: 76, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 7, video_link: "https://youtube.com/embed/qci3ODe_fLY"
    },
    {
        sn: 77, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 8, video_link: "https://youtube.com/embed/AtXp6KQGfNo"
    },
    {
        sn: 78, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 9, video_link: "https://youtube.com/embed/flnpQnpNSkQ"
    },
    {
        sn: 79, class: "X", subject: "Chemistry", chapter_name: "Carbon and it's Compounds", lecture_number: 10, video_link: "https://youtube.com/embed/gsVQNnnDU3g"
    },
    {
        sn: 80, class: "X", subject: "Chemistry", chapter_name: "Periodic classification of Elements", lecture_number: 1, video_link: "https://youtube.com/embed/WsJuCgxCji0"
    },
    {
        sn: 81, class: "X", subject: "Chemistry", chapter_name: "Periodic classification of Elements", lecture_number: 2, video_link: "https://youtube.com/embed/UuW0oKG4h1w"
    },
    {
        sn: 82, class: "X", subject: "Chemistry", chapter_name: "Periodic classification of Elements", lecture_number: 3, video_link: "https://youtube.com/embed/RXIlC3i6oKk"
    },
    {
        sn: 83, class: "X", subject: "Chemistry", chapter_name: "Periodic classification of Elements", lecture_number: 4, video_link: "https://youtube.com/embed/wkfE7ytkBkc"
    },
    {
        sn: 84, class: "X", subject: "Chemistry", chapter_name: "Periodic classification of Elements", lecture_number: 5, video_link: "https://youtube.com/embed/kW-F3BtitlQ"
    },
    {
        sn: 85, class: "X", subject: "Mathematics", chapter_name: "Real Numbers", lecture_number: 1, video_link: "https://youtube.com/embed/EstB5T4V8Ls"
    },
    {
        sn: 86, class: "X", subject: "Mathematics", chapter_name: "Real Numbers", lecture_number: 2, video_link: "https://youtube.com/embed/c7vtGzKHfPw"
    },
    {
        sn: 87, class: "X", subject: "Mathematics", chapter_name: "Real Numbers", lecture_number: 3, video_link: "https://youtube.com/embed/RrupzC1aXCM"
    },
    {
        sn: 88, class: "X", subject: "Mathematics", chapter_name: "Real Numbers", lecture_number: 4, video_link: "https://youtube.com/embed/RKDI29aFv34"
    },
    {
        sn: 89, class: "X", subject: "Mathematics", chapter_name: "Real Numbers", lecture_number: 5, video_link: "https://youtube.com/embed/7hnWAoldJrw"
    },
    {
        sn: 90, class: "X", subject: "Mathematics", chapter_name: "Real Numbers", lecture_number: 6, video_link: "https://youtube.com/embed/Xxjwonfgkas"
    },
    {
        sn: 91, class: "X", subject: "Mathematics", chapter_name: "Polynomials", lecture_number: 1, video_link: "https://youtube.com/embed/nS67F3xroXc"
    },
    {
        sn: 92, class: "X", subject: "Mathematics", chapter_name: "Polynomials", lecture_number: 2, video_link: "https://youtube.com/embed/35Yt3CqgU9U"
    },
    {
        sn: 93, class: "X", subject: "Mathematics", chapter_name: "Polynomials", lecture_number: 3, video_link: "https://youtube.com/embed/zPLRZsu38qA"
    },
    {
        sn: 94, class: "X", subject: "Mathematics", chapter_name: "Polynomials", lecture_number: 4, video_link: "https://youtube.com/embed/9yOpS2ty2kI"
    },
    {
        sn: 95, class: "X", subject: "Mathematics", chapter_name: "Polynomials", lecture_number: 5, video_link: "https://youtube.com/embed/jLLULnonIX8"
    },
    {
        sn: 96, class: "X", subject: "Mathematics", chapter_name: "Polynomials", lecture_number: 6, video_link: "https://youtube.com/embed/5EpIpFsgabc"
    },
    {
        sn: 97, class: "X", subject: "Mathematics", chapter_name: "Pair of Linear equation in two variable", lecture_number: 1, video_link: "https://youtube.com/embed/JHXoNs_AC5Q"
    },
    {
        sn: 98, class: "X", subject: "Mathematics", chapter_name: "Pair of Linear equation in two variable", lecture_number: 2, video_link: "https://youtube.com/embed/mqZ15wD36zs"
    },
    {
        sn: 99, class: "X", subject: "Mathematics", chapter_name: "Pair of Linear equation in two variable", lecture_number: 3, video_link: "https://youtube.com/embed/4ddoT_UiYiI"
    },
    {
        sn: 100, class: "X", subject: "Mathematics", chapter_name: "Pair of Linear equation in two variable", lecture_number: 4, video_link: "https://youtube.com/embed/n0J6BMfGGEI"
    },
    {
        sn: 101, class: "X", subject: "Mathematics", chapter_name: "Pair of Linear equation in two variable", lecture_number: 5, video_link: "https://youtube.com/embed/NLhJYE1eFn4"
    },
    {
        sn: 102, class: "X", subject: "Mathematics", chapter_name: "Pair of Linear equation in two variable", lecture_number: 6, video_link: "https://youtube.com/embed/XpEJIT-QsMA"
    },
    {
        sn: 103, class: "X", subject: "Mathematics", chapter_name: "Quadratic Equation", lecture_number: 1, video_link: "https://youtube.com/embed/EYDeVjztbjI"
    },
    {
        sn: 104, class: "X", subject: "Mathematics", chapter_name: "Quadratic Equation", lecture_number: 2, video_link: "https://youtube.com/embed/Ea43xhoyNWs"
    },
    {
        sn: 105, class: "X", subject: "Mathematics", chapter_name: "Quadratic Equation", lecture_number: 3, video_link: "https://youtube.com/embed/6tobHXvdgrw"
    },
    {
        sn: 106, class: "X", subject: "Mathematics", chapter_name: "Quadratic Equation", lecture_number: 4, video_link: "https://youtube.com/embed/Yd79v3Y1I0U"
    },
    {
        sn: 107, class: "X", subject: "Mathematics", chapter_name: "Quadratic Equation", lecture_number: 5, video_link: "https://youtube.com/embed/EDJHvF5yBS0"
    },
    {
        sn: 108, class: "X", subject: "Mathematics", chapter_name: "Quadratic Equation", lecture_number: 6, video_link: "https://youtube.com/embed/ZX3ifzbgO1E"
    },
    {
        sn: 109, class: "X", subject: "Mathematics", chapter_name: "Arithmetic Progressions", lecture_number: 1, video_link: "https://youtube.com/embed/8u4NgB0Pyvg"
    },
    {
        sn: 110, class: "X", subject: "Mathematics", chapter_name: "Arithmetic Progressions", lecture_number: 2, video_link: "https://youtube.com/embed/DmQTooveMjQ"
    },
    {
        sn: 111, class: "X", subject: "Mathematics", chapter_name: "Arithmetic Progressions", lecture_number: 3, video_link: "https://youtube.com/embed/RLBGAu9x7FA"
    },
    {
        sn: 112, class: "X", subject: "Mathematics", chapter_name: "Arithmetic Progressions", lecture_number: 4, video_link: "https://youtube.com/embed/Onvme95RD9U"
    },
    {
        sn: 113, class: "X", subject: "Mathematics", chapter_name: "Coordinate Geometry", lecture_number: 1, video_link: "https://youtube.com/embed/LrBnpjUWJ20"
    },
    {
        sn: 114, class: "X", subject: "Mathematics", chapter_name: "Coordinate Geometry", lecture_number: 2, video_link: "https://youtube.com/embed/VhkkZdWDzHw"
    },
    {
        sn: 115, class: "X", subject: "Mathematics", chapter_name: "Coordinate Geometry", lecture_number: 3, video_link: "https://youtube.com/embed/dCjrL88ByMk"
    },
    {
        sn: 116, class: "X", subject: "Mathematics", chapter_name: "Coordinate Geometry", lecture_number: 4, video_link: "https://youtube.com/embed/sXKJjWA6ocw"
    },
    {
        sn: 117, class: "X", subject: "Mathematics", chapter_name: "Coordinate Geometry", lecture_number: 5, video_link: "https://youtube.com/embed/XYXysliRC7c"
    },
    {
        sn: 118, class: "X", subject: "Mathematics", chapter_name: "Triangles", lecture_number: 1, video_link: "https://youtube.com/embed/O9J9MWcJG18"
    },
    {
        sn: 119, class: "X", subject: "Mathematics", chapter_name: "Triangles", lecture_number: 2, video_link: "https://youtube.com/embed/52K0b6Z2lok"
    },
    {
        sn: 120, class: "X", subject: "Mathematics", chapter_name: "Triangles", lecture_number: 3, video_link: "https://youtube.com/embed/2EWALCcg3fE"
    },
    {
        sn: 121, class: "X", subject: "Mathematics", chapter_name: "Triangles", lecture_number: 4, video_link: "https://youtube.com/embed/r0jbwH9hFw0"
    },
    {
        sn: 122, class: "X", subject: "Mathematics", chapter_name: "Triangles", lecture_number: 5, video_link: "https://youtube.com/embed/rWYXUpaa2uo"
    },
    {
        sn: 123, class: "X", subject: "Mathematics", chapter_name: "Triangles", lecture_number: 6, video_link: "https://youtube.com/embed/bpDvHsL4rzE"
    },
    {
        sn: 124, class: "X", subject: "Mathematics", chapter_name: "Introduction to Trigonometry", lecture_number: 1, video_link: "https://youtube.com/embed/2Hd_kgwMhy8"
    },
    {
        sn: 125, class: "X", subject: "Mathematics", chapter_name: "Introduction to Trigonometry", lecture_number: 2, video_link: "https://youtube.com/embed/DjWu2CLtD4w"
    },
    {
        sn: 126, class: "X", subject: "Mathematics", chapter_name: "Introduction to Trigonometry", lecture_number: 3, video_link: "https://youtube.com/embed/fQ27MlzQmjk"
    },
    {
        sn: 127, class: "X", subject: "Mathematics", chapter_name: "Introduction to Trigonometry", lecture_number: 4, video_link: "https://youtube.com/embed/eadgb4uDSLY"
    },
    {
        sn: 128, class: "X", subject: "Mathematics", chapter_name: "Some Applications of Trigonometry", lecture_number: 1, video_link: "https://youtube.com/embed/j4OiblLZoF4"
    },
    {
        sn: 129, class: "X", subject: "Mathematics", chapter_name: "Some Applications of Trigonometry", lecture_number: 2, video_link: "https://youtube.com/embed/HH-JH7k8zd8"
    },
    {
        sn: 130, class: "X", subject: "Mathematics", chapter_name: "Circles", lecture_number: 1, video_link: "https://youtube.com/embed/3vxUV11tLGI"
    },{
        sn: 131, class: "X", subject: "Mathematics", chapter_name: "Circles", lecture_number: 2, video_link: "https://youtube.com/embed/jlo1JdWhznA"
    },
    {
        sn: 132, class: "X", subject: "Mathematics", chapter_name: "Circles", lecture_number: 3, video_link: "https://youtube.com/embed/38aLZDkmUvU"
    },
    {
        sn: 133, class: "X", subject: "Mathematics", chapter_name: "Constructions", lecture_number: 1, video_link: "https://youtube.com/embed/3xeqNrbT3ak"
    },
    {
        sn: 134, class: "X", subject: "Mathematics", chapter_name: "Areas Related to Circles", lecture_number: 1, video_link: "https://youtube.com/embed/QYWTZKUCqv4"
    },
    {
        sn: 135, class: "X", subject: "Mathematics", chapter_name: "Areas Related to Circles", lecture_number: 2, video_link: "https://youtube.com/embed/bE1NgE5l5LA"
    },
    {
        sn: 136, class: "X", subject: "Mathematics", chapter_name: "Statistics", lecture_number: 1, video_link: "https://youtube.com/embed/CGYeawrEW0c"
    },
    {
        sn: 137, class: "X", subject: "Mathematics", chapter_name: "Statistics", lecture_number: 2, video_link: "https://youtube.com/embed/LahMscT5C5s"
    },
    {
        sn: 138, class: "X", subject: "Mathematics", chapter_name: "Statistics", lecture_number: 3, video_link: "https://youtube.com/embed/mIj7ZcRWkSE"
    },
    {
        sn: 139, class: "X", subject: "Mathematics", chapter_name: "Statistics", lecture_number: 4, video_link: "https://youtube.com/embed/wDJn6Vnocus"
    },
    {
        sn: 140, class: "X", subject: "Mathematics", chapter_name: "Statistics", lecture_number: 5, video_link: "https://youtube.com/embed/b6IaYlW4kJc"
    },
    {
        sn: 141, class: "X", subject: "Mathematics", chapter_name: "Surface Areas and Volumes", lecture_number: 1, video_link: "https://youtube.com/embed/Qt0RycOwB-o"
    },
    {
        sn: 142, class: "X", subject: "Mathematics", chapter_name: "Surface Areas and Volumes", lecture_number: 2, video_link: "https://youtube.com/embed/hCVsIqpmtZY"
    },
    {
        sn: 143, class: "X", subject: "Mathematics", chapter_name: "Surface Areas and Volumes", lecture_number: 3, video_link: "https://youtube.com/embed/q1EyHPJggHo"
    },
    {
        sn: 144, class: "X", subject: "Mathematics", chapter_name: "Surface Areas and Volumes", lecture_number: 4, video_link: "https://youtube.com/embed/GFLPGau8Iec"
    },
    {
        sn: 145, class: "X", subject: "Mathematics", chapter_name: "Surface Areas and Volumes", lecture_number: 5, video_link: "https://youtube.com/embed/piTDZz6nt4Y"
    },
    {
        sn: 146, class: "X", subject: "Mathematics", chapter_name: "Surface Areas and Volumes", lecture_number: 6, video_link: "https://youtube.com/embed/dHvwp6DNOLs"
    },
    {
        sn: 147, class: "X", subject: "Mathematics", chapter_name: "Probability", lecture_number: 1, video_link: "https://youtube.com/embed/rJ61WUmD1CQ"
    },
    {
        sn: 148, class: "X", subject: "Mathematics", chapter_name: "Probability", lecture_number: 2, video_link: "https://youtube.com/embed/3OxN4rWATSk"
    },
    {
        sn: 149, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 1, video_link: "https://youtube.com/embed/BVqyHC4coiM"
    },
    {
        sn: 150, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 2, video_link: "https://youtube.com/embed/3ob9MpyHlpE"
    },
    {
        sn: 151, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 3, video_link: "https://youtube.com/embed/IRcDpHHguXM"
    },
    {
        sn: 152, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 4, video_link: "https://youtube.com/embed/V-kmZVE85wA"
    },
    {
        sn: 153, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 5, video_link: "https://youtube.com/embed/xYwNdpsWCkA"
    },
    {
        sn: 154, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 6, video_link: "https://youtube.com/embed/hdZT_XZ3aqo"
    },
    {
        sn: 155, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 7, video_link: "https://youtube.com/embed/bLBPt5rxFBE"
    },
    {
        sn: 156, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 8, video_link: "https://youtube.com/embed/REqy8mWMn6E"
    },
    {
        sn: 157, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 9, video_link: "https://youtube.com/embed/zRuBuyXSz-o"
    },
    {
        sn: 158, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 10, video_link: "https://youtube.com/embed/9JolpJ0mR3Q"
    },
    {
        sn: 159, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 11, video_link: "https://youtube.com/embed/hd-LJKhC134"
    },
    {
        sn: 160, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 12, video_link: "https://youtube.com/embed/6tazE-Oi9U8"
    },
    {
        sn: 161, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 13, video_link: "https://youtube.com/embed/wvZdtfs0QE4"
    },
    {
        sn: 162, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 14, video_link: "https://youtube.com/embed/Lc4ZwWkFIYU"
    },
    {
        sn: 163, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 15, video_link: "https://youtube.com/embed/ow-Q8zzpRXI"
    },
    {
        sn: 164, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 16, video_link: "https://youtube.com/embed/IAndi3yFCS4"
    },
    {
        sn: 165, class: "X", subject: "Biology", chapter_name: "Life Processes", lecture_number: 17, video_link: "https://youtube.com/embed/itQ0IUIbLK0"
    },
    {
        sn: 166, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 1, video_link: "https://youtube.com/embed/I8_jCcMOgJ8"
    },
    {
        sn: 167, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 2, video_link: "https://youtube.com/embed/GHiCGMq5oA8"
    },
    {
        sn: 168, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 3, video_link: "https://youtube.com/embed/darM5xt8DvY"
    },
    {
        sn: 169, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 4, video_link: "https://youtube.com/embed/3fckU9CoBac"
    },
    {
        sn: 170, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 5, video_link: "https://youtube.com/embed/5GuT9UErO90"
    },
    {
        sn: 171, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 6, video_link: "https://youtube.com/embed/IEfwuARk8e8"
    },
    {
        sn: 172, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 7, video_link: "https://youtube.com/embed/m-hdFOnX1Uc"
    },
    {
        sn: 173, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 8, video_link: "https://youtube.com/embed/-bnnxIJj6WU"
    },
    {
        sn: 174, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 9, video_link: "https://youtube.com/embed/iiNn2vPrisw"
    },
    {
        sn: 175, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 10, video_link: "https://youtube.com/embed/Z8pSEVLR5YM"
    },
    {
        sn: 176, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 11, video_link: "https://youtube.com/embed/RA5n7TDzVOE"
    },
    {
        sn: 177, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 12, video_link: "https://youtube.com/embed/kemmLzETrrg"
    },
    {
        sn: 178, class: "X", subject: "Biology", chapter_name: "Control and Coordination", lecture_number: 13, video_link: "https://youtube.com/embed/iirhx4cTKaU"
    },
    {
        sn: 179, class: "X", subject: "Biology", chapter_name: "How do Organism Reproduce?", lecture_number: 1, video_link: "https://youtube.com/embed/EjLS3luPsm0"
    },
    {
        sn: 180, class: "X", subject: "Biology", chapter_name: "How do Organism Reproduce?", lecture_number: 2, video_link: "https://youtube.com/embed/pEwAJCkHg7M"
    },
    {
        sn: 181, class: "X", subject: "Biology", chapter_name: "How do Organism Reproduce?", lecture_number: 3, video_link: "https://youtube.com/embed/Ff-PHkX9oHo"
    },
    {
        sn: 182, class: "X", subject: "Biology", chapter_name: "How do Organism Reproduce?", lecture_number: 4, video_link: "https://youtube.com/embed/eTjRWKX9Ajc"
    },
    {
        sn: 183, class: "X", subject: "Biology", chapter_name: "How do Organism Reproduce?", lecture_number: 5, video_link: "https://youtube.com/embed/VjhV227nppI"
    },
    {
        sn: 184, class: "X", subject: "Biology", chapter_name: "How do Organism Reproduce?", lecture_number: 6, video_link: "https://youtube.com/embed/mLhXgOF6hB0"
    },
    {
        sn: 185, class: "X", subject: "Biology", chapter_name: "How do Organism Reproduce?", lecture_number: 7, video_link: "https://youtube.com/embed/3FEybwGfAgo"
    },
    {
        sn: 186, class: "X", subject: "Biology", chapter_name: "Heredity and Evolution", lecture_number: 1, video_link: "https://youtube.com/embed/iChzDjAcNw8"
    },
    {
        sn: 187, class: "X", subject: "Biology", chapter_name: "Heredity and Evolution", lecture_number: 2, video_link: "https://youtube.com/embed/ZmwN066StRI"
    },
    {
        sn: 188, class: "X", subject: "Biology", chapter_name: "Heredity and Evolution", lecture_number: 3, video_link: "https://youtube.com/embed/HcmkEaZffjI"
    },
    {
        sn: 189, class: "X", subject: "Biology", chapter_name: "Heredity and Evolution", lecture_number: 4, video_link: "https://youtube.com/embed/fdJxDdxOKoM"
    },
    {
        sn: 190, class: "X", subject: "Biology", chapter_name: "Heredity and Evolution", lecture_number: 5, video_link: "https://youtube.com/embed/_6Txa7l8sqI"
    },
    {
        sn: 191, class: "X", subject: "Biology", chapter_name: "Heredity and Evolution", lecture_number: 6, video_link: "https://youtube.com/embed/fOS6cd7iveY"
    },
    {
        sn: 192, class: "X", subject: "Biology", chapter_name: "Heredity and Evolution", lecture_number: 7, video_link: "https://youtube.com/embed/4SEjT4yN2Z0"
    },
    {
        sn: 193, class: "X", subject: "Biology", chapter_name: "Our Environment", lecture_number: 1, video_link: "https://youtube.com/embed/2EaFAlmvmFk"
    },
    {
        sn: 194, class: "X", subject: "Biology", chapter_name: "Our Environment", lecture_number: 2, video_link: "https://youtube.com/embed/_6XReSgnbpU"
    },
    {
        sn: 195, class: "X", subject: "Biology", chapter_name: "Our Environment", lecture_number: 3, video_link: "https://youtube.com/embed/gm4XPIJ3AEM"
    },
    {
        sn: 196, class: "X", subject: "Biology", chapter_name: "Our Environment", lecture_number: 4, video_link: "https://youtube.com/embed/7qGRtBH99cQ"
    },
    {
        sn: 197, class: "X", subject: "Biology", chapter_name: "Our Environment", lecture_number: 5, video_link: "https://youtube.com/embed/7Av9Bs_riQ4"
    },
    {
        sn: 198, class: "X", subject: "Biology", chapter_name: "Management of Natural Resources", lecture_number: 1, video_link: "https://youtube.com/embed/oGX7SwMq-SA"
    },
    {
        sn: 199, class: "X", subject: "Biology", chapter_name: "Management of Natural Resources", lecture_number: 2, video_link: "https://youtube.com/embed/LFnVqk0iNzY"
    },
    {
        sn: 200, class: "X", subject: "Biology", chapter_name: "Management of Natural Resources", lecture_number: 3, video_link: "https://youtube.com/embed/HBulG6JRZt0"
    } 
]

module.exports = { data: classXData };