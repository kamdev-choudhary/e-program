const express = require("express");
const app = express();
const path = require("path");
const mongoose = require("mongoose");
const Listing = require("../e-program/models/listings.js");
const methodOverride = require("method-override");
const ejsMate = require("ejs-mate");
const mongo_url = "mongodb://127.0.0.1:27017/dakshana";

main().then(()=>{
    console.log(`conneted to DB ${mongo_url}`);
}).catch((err)=>{
    console.log(err);
});

async function main() {
    await mongoose.connect(mongo_url);
}

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.urlencoded({ extended: true }));
app.use(methodOverride("_method"));
app.engine('ejs', ejsMate);
app.use(express.static(__dirname + '/public'));


//Home Route
app.get("/", (req, res) => {
    res.render("home.ejs");
});


//syllbus route
app.get("/syllabus",(req, res) => {

    res.render("syllabus.ejs");
});


// Exam Route
app.get("/exams", (req, res) => {
    res.render("exams.ejs")
});


//Lecture Route
app.get("/lectures", async (req, res) => {
    const listings = await Listing.find({}); 
    res.render("lectures.ejs", {listings});
});


app.get("/lectures/physics", async (req, res) => {
    let sub = "PHYSICS";
    const listings = await Listing.find({subject : "Physics"}); 
    res.render("lectures.ejs", {listings, sub});
});


app.get("/lectures/chemistry", async (req, res) => {
    let sub = "CHEMISTRY";
    const listings = await Listing.find({subject : "Chemistry"}); 
    res.render("lectures.ejs", {listings, sub});
});

app.get("/lectures/maths", async (req, res) => {
    let sub = "MATHEMATICS";
    const listings = await Listing.find({subject : "Mathematics"}); 
    res.render("lectures.ejs", {listings, sub});
});

app.get("/lectures/biology", async (req, res) => {
    let sub = "BIOLOGY";
    const listings = await Listing.find({subject : "Biology"}); 
    res.render("lectures.ejs", {listings, sub});
});


app.listen(8080, (req, res) =>{
    console.log("server is listening at port : 8080");
});
